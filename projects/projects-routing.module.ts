import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './../layout/layout.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { JobChartComponent } from './job-chart/job-chart.component';
import { JobCreateComponent } from './job-create/job-create.component';
import { Form0Component } from './job-create/form0/form0.component';
import { Form1Component } from './job-create/form1/form1.component';

const routes: Routes = [{
    path: 'app',
    component: LayoutComponent,
    children: [
        { path: 'project-list', component: ProjectListComponent },        
        { path: 'job/:jobId/chart', component: JobChartComponent },
        { path: 'project/:projectId/job/new/type', component: JobCreateComponent, children: [
            { path: '0', component: Form0Component },
            { path: '1', component: Form1Component }            
        ]},
    ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
