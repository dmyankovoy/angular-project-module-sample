import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

import { ProjectsService } from './../projects.service';

import { Project, Token } from './../models/index';

@Component({
    selector: 'app-token-get',
    templateUrl: './token-get.component.html',
})
export class TokenGetComponent implements OnInit {
    token: Token;

    constructor(public dialogRef: MdDialogRef<TokenGetComponent>,
        private projectsService: ProjectsService,
        @Inject(MD_DIALOG_DATA) private projectId: string) {
    }

    ngOnInit() {
        this.projectsService.getToken(this.projectId).subscribe((token: Token) => {
            this.token = token;
        })
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
