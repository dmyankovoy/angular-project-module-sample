import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { MdDatepickerModule, MdNativeDateModule  } from '@angular/material';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectsService } from './projects.service';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { TokenGetComponent } from './token-get/token-get.component';
import { JobCreateComponent } from './job-create/job-create.component';
import { JobChartComponent } from './job-chart/job-chart.component';

import { AuthModule } from './../auth/auth.module';

import { Ng2EchartsDirective } from './../echarts/ng2-echarts';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { CollaboratorsManageComponent } from './collaborators-manage/collaborators-manage.component';
import { Form0Component } from './job-create/form0/form0.component';
import { Form1Component } from './job-create/form1/form1.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        MdDatepickerModule,
        MdNativeDateModule,
        ProjectsRoutingModule,        
        AuthModule        
    ],
    declarations: [
        ProjectListComponent, 
        ProjectCreateComponent, 
        TokenGetComponent, 
        JobCreateComponent, 
        JobChartComponent,
        Ng2EchartsDirective,
        ProjectEditComponent,
        CollaboratorsManageComponent,
        Form0Component,
        Form1Component
        ],
    providers: [ProjectsService],
    entryComponents: [ProjectCreateComponent, ProjectEditComponent, CollaboratorsManageComponent, TokenGetComponent]
})
export class ProjectsModule { }
