import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Project } from './../models/index';

@Component({
    selector: 'app-project-edit',
    templateUrl: './project-edit.component.html'
})
export class ProjectEditComponent {
    projectForm: FormGroup;

    constructor(private dialogRef: MdDialogRef<ProjectEditComponent>,
        private formBuilder: FormBuilder,
        @Inject(MD_DIALOG_DATA) private project: Project) {

        this.projectForm = formBuilder.group({
            name: [project.name, [
                Validators.minLength(3),
                Validators.required
            ]],
            description: [project.description]
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    submit(project: Project): void {
        this.dialogRef.close(project);
    }
}
