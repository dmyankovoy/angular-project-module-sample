import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth/auth.service';

import { ProjectsService } from '../projects.service';
import { JobChartDataItem } from '../models/job-data-chart.model';

interface StatView {
    key: string,
    val: any;
}

interface ChartViewModel {
    title: string,
    stats: StatView[],
    // config for Echarts instance
    chartOptions: any;
}

@Component({
    selector: 'app-job-chart',
    templateUrl: './job-chart.component.html'
})
export class JobChartComponent implements OnInit {

    constructor(private projectsService: ProjectsService,
        private route: ActivatedRoute,
        private authService: AuthService) { }

    results: ChartViewModel[] = [];
    logs: string;
    // -1: logs
    activeTab: number = -1;

    get logsAllowed(): boolean {
        return this.authService.isAdmin() && !!this.logs;
    }

    // prepare ECharts config
    static getChartParams(chartData: JobChartDataItem[]): Object {
        const chartPoints = chartData.map(x => {
            return [x.date, x.pnL];
        });

        return {
            theme: 'custom',
            yAxis: [{
                type: 'value',
                scale: true,
            }],
            xAxis: {
                type: 'time',
            },
            axisPointer: {
                snap: false,
                show: true,
                triggerTooltip: false
            },
            tooltip: {
                trigger: 'none',
            },
            animation: true,
            series: [{
                type: 'line',
                smooth: true,
                clipOverflow: false,
                name: 'data',
                symbolSize: 0,
                data: chartPoints,
                itemStyle: {
                    normal: {
                        lineStyle: {
                            width: 2,
                            type: 'solid'
                        }
                    }
                }
            }]
        };
    }

    loadCharts(jobId: string): void {
        let reg = new RegExp('^Prediction\\w+\\.csv$');

        this.projectsService.getJobFiles(jobId).subscribe(jobResultFiles => {
            if (jobResultFiles.length === 0) {
                this.results = [];
                return;
            }

            let fileNames: string[] = [];

            for (let fileName of jobResultFiles) {
                if (reg.test(fileName)) {
                    fileNames.push(fileName);
                }
            };

            this.projectsService.getJobResults(jobId, fileNames).subscribe(chartsData => {
                for (let chartData of chartsData) {
                    const statsPairs: StatView[] = Object.keys(chartData.stat[0]).map(key => {
                        return { key: key, val: chartData.stat[0][key] };
                    });

                    this.results.push({
                        title: chartData.fileName,
                        chartOptions: JobChartComponent.getChartParams(chartData.chart),
                        stats: statsPairs
                    });
                }
            });
        });
    }

    loadLogs(jobId: string): void {
        if (this.authService.isAdmin()) {
            this.projectsService.getJobLogs(jobId).subscribe(logsText => this.logs = logsText);
        }
    }

    ngOnInit() {
        let jobId: string = this.route.snapshot.params['jobId'];

        this.loadLogs(jobId);
        this.loadCharts(jobId);
    }
}
