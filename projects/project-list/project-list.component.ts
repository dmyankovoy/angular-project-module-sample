import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

import { ProjectsService } from './../projects.service';
import { ProjectCreateComponent } from './../project-create/project-create.component';
import { ProjectEditComponent } from './../project-edit/project-edit.component';
import { CollaboratorsManageComponent } from './../collaborators-manage/collaborators-manage.component';
import { TokenGetComponent } from './../token-get/token-get.component';

import { Project } from './../models/index';

@Component({
    selector: 'app-project-list',
    templateUrl: './project-list.component.html',
})
export class ProjectListComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private projectsService: ProjectsService,
        private createUserDialog: MdDialog,
        private editProjectDialog: MdDialog,
        private getTokenDialog: MdDialog) { }

    private projects: Array<Project>;
    private newJobRoute: string = '';

    loadProjects(projectId) {
        this.projectsService.getProjects().subscribe(res => {
            this.projects = res;
            if (projectId) {
                this.projects.forEach(x => {

                    if (projectId === x.id) {
                        this.toggleJobs(x);
                    }
                })
            }
        });
    }

    ngOnInit() {
        this.route
            .queryParams
            .subscribe(params => {
                // Defaults to 0 if no query param provided.            
                this.loadProjects(params['project']);
                this.projectsService.getAlgs().subscribe(algs => {
                    if (algs.length > 0) {
                        this.newJobRoute = `${algs[0].formVersion}`;
                    }
                });
            });
    }

    openCreateProjectDialog() {
        let dialogRef = this.getTokenDialog.open(ProjectCreateComponent, {
            width: '450px'
        });

        dialogRef.afterClosed().subscribe((res: Project) => {
            if (res) {
                this.projectsService.createProject(res.name, res.description).subscribe(res => {
                    this.loadProjects(null);
                });
            }
        });
    }

    openGetTokenDialog(projectId) {
        let dialogRef = this.createUserDialog.open(TokenGetComponent, {
            width: '450px',
            data: projectId
        });
    }

    toggleJobs(project) {
        project.jobsOpened = !project.jobsOpened;
        if (project.jobsOpened) {
            this.refresh(project);
        }
    }

    refresh(project) {
        this.projectsService.getJobs(project.id).subscribe(res => {
            project.jobs = res;
        });
    }

    editProject(project) {
        let dialogRef = this.editProjectDialog.open(ProjectEditComponent, {
            width: '450px',
            data: {
                name: project.name,
                description: project.description
            }
        });

        dialogRef.afterClosed().subscribe(data => {
            this.projectsService.updateProject(project.id, data.name, data.description).subscribe(res => {
                project.name = data.name;
                project.description = data.description;
            });
        });
    }

    manageCollaborators(project) {
        let dialogRef = this.editProjectDialog.open(CollaboratorsManageComponent, {
            width: '600px',
            data: {
                project: project
            }
        });

        dialogRef.afterClosed().subscribe(data => { });
    }

    deleteJob(job, project) {
        this.projectsService.deleteJob(job.id).subscribe(res => {
            this.refresh(project);
        });
    }
}
