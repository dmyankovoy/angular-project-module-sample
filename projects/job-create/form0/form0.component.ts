import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Observable, Subscription } from 'rxjs';
import { MdSnackBar } from '@angular/material';

import { ProjectsService } from './../../projects.service';

import { AbstractFormComponent } from './../job-create-abstract-form.component';
import { Job, JobInput, JobInputType0 } from './../../models';

@Component({
    selector: 'app-job-create-form0',
    templateUrl: './form0.component.html'
})
export class Form0Component extends AbstractFormComponent {

    constructor(protected projectsService: ProjectsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected formBuilder: FormBuilder,
        protected snackBar: MdSnackBar) {

        super(projectsService, route, router, formBuilder, snackBar);

        const classValidators = [Validators.required, Validators.min(0), Validators.max(1)];
        const noiseValidators = [Validators.required, Validators.min(0), Validators.max(1)];

        this.jobForm.addControl('mutualInfo', this.formBuilder.control(false));
        this.jobForm.addControl('negClass', this.formBuilder.control(0.3, classValidators));
        this.jobForm.addControl('negNoise', this.formBuilder.control(0.05, noiseValidators));
        this.jobForm.addControl('posClass', this.formBuilder.control(0.3, classValidators));
        this.jobForm.addControl('posNoise', this.formBuilder.control(0.05, noiseValidators));
    }

    restoreForm(job: Job): void {
        const input = <JobInputType0>job.input;
        this.jobForm.controls['mutualInfo'].setValue(input.method === "MutualInfo");
        this.jobForm.controls['negClass'].setValue(input.negClass);
        this.jobForm.controls['negNoise'].setValue(input.negNoise);
        this.jobForm.controls['posClass'].setValue(input.posClass);
        this.jobForm.controls['posNoise'].setValue(input.posNoise);
    }

    getPayload(payload: Job, formData: any): Job {
        payload.formVersion = 0;

        let input = <JobInputType0>payload.input;
        input.method = formData.mutualInfo ? "MutualInfo" : "NNMF";
        input.negNoise = formData.negNoise;
        input.posNoise = formData.posNoise;
        input.negClass = formData.negClass;
        input.posClass = formData.posClass;

        return payload;
    }
}