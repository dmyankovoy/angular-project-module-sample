const horizonVals = [1, 2, 3, 4, 5];
const vsVals = ['Daily', 'Intraday', 'Overnight'];
const indices = ['Index', 'Equities', 'Fx', 'Commodities', 'Interest rates'];
const indicesTypes = ['Fundamentals', 'Technicals', 'Proprietary'];
const optionsVS = {
    'Daily': 'CtoC',
    'Intraday': 'OtoC',
    'Overnight': 'CtoO'
};

const jobFormParams = {
    vsVals,
    horizonVals,
    indices,
    indicesTypes,
    optionsVS
};

export { jobFormParams };