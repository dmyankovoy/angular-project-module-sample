import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, Subject } from 'rxjs';

import { ProjectsService } from './../projects.service';
import { Algorithm } from './../models/index';

@Component({
    selector: 'app-job-create',
    templateUrl: './job-create.component.html'
})
export class JobCreateComponent implements OnInit {

    model: any = {
        alg: null
    };

    constructor(private projectsService: ProjectsService,
        private route: ActivatedRoute,
        private router: Router) { }

    algs: any[];
    projectId: string;

    onAlgChange(e) {
        for (let a of this.algs) {
            if (a.name === e.value) {
                this.projectsService.selectAlgorithm(a.name);
                this.router.navigate([`/app/project/${this.projectId}/job/new/type/${a.formVersion}`]);
            }
        };
    }

    ngOnInit() {
        this.projectId = this.route.snapshot.params['projectId'];
        this.projectsService.getAlgs().subscribe((algs: Algorithm[]) => {
            this.algs = algs;

            const clone = this.route.snapshot.queryParams['clone'];
            if (clone) {
                this.projectsService.getJobInfo(this.projectId, clone).subscribe(job => {
                    if (job && job.input) {
                        this.projectsService.selectAlgorithm(job.algorithm);
                        this.model.alg = job.algorithm;
                    }
                });
            } else {
                // set first algoritm selected
                if (algs.length > 0) {
                    this.projectsService.selectAlgorithm(algs[0].name);
                    this.model.alg = algs[0].name;
                }
            }
        });
    }
}
