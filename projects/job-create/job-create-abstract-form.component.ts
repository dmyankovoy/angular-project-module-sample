import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Observable, Subscription } from 'rxjs';
import { MdSnackBar } from '@angular/material';

import { ProjectsService } from './../projects.service';
import { JobCreateComponent } from './job-create.component';
import { jobFormParams } from './job-form-options';

@Component({})
export abstract class AbstractFormComponent implements OnInit, OnDestroy {
    protected jobForm: FormGroup;

    protected alg: string;
    protected jobFormParams = jobFormParams;
    protected projectId: string;
    private assetGroups: any[] = [];
    private assets: any[] = [];
    private algChabgeSubscription: Subscription;
    private showOptions: boolean = false;

    constructor(protected projectsService: ProjectsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected formBuilder: FormBuilder,
        protected snackBar: MdSnackBar) {

        this.jobForm = formBuilder.group({
            assetGroup: [''],
            asset: ['', [Validators.required]],
            date: [new Date(), [Validators.required]],
            horizon: [1],
            vs: ['Daily'],
            calculPnl: [0],
            replaceModel: [false],
            replaceBase: [false]
        });
    }

    abstract restoreForm(jobInfo);

    restoreFormBase(sourceJobId, assetsData) {
        this.projectsService.getJobInfo(this.projectId, sourceJobId).subscribe(job => {
            if (job && job.input) {
                const input = job.input;
                const vsMatch = input.VSName.match(/^VS_Ret_(\w+)_(\d+)d$/);
                Object.keys(this.jobFormParams.optionsVS).forEach((key) => {
                    if (this.jobFormParams.optionsVS[key] === vsMatch[1]) {
                        this.jobForm.controls['vs'].setValue(key);
                    }
                });

                for (let assetGroup of assetsData) {
                    assetGroup.items.forEach(item => {
                        if (item.name === input.assetName) {
                            this.jobForm.controls['assetGroup'].setValue(assetGroup.name);
                            this.changeAssets(assetGroup.name);
                            this.jobForm.controls['asset'].setValue(input.assetName);
                        }
                    })
                };

                this.jobForm.controls['horizon'].setValue(parseInt(vsMatch[2]));
                this.jobForm.controls['date'].setValue(new Date(input.begDateTest));
                this.jobForm.controls['replaceBase'].setValue(input.boolReplaceBase);
                this.jobForm.controls['replaceModel'].setValue(input.boolReplaceModel);

                this.restoreForm(job);
            }
        });
    }

    changeAssets(category) {
        this.assetGroups.forEach(group => {
            if (category === group.name) {
                this.assets = group.items;
                if (group.items.length > 0) {
                    this.jobForm.controls['asset'].setValue(group.items[0].name);
                }
            }
        });
    }

    onAssetGroupChange() {
        this.changeAssets(this.jobForm.controls['assetGroup'].value);
    }

    abstract getPayload(payload, form);

    getPayloadBase(form) {
        const date = form.date;
        date.setHours(date.getHours() - date.getTimezoneOffset() / 60);

        return {
            algorithm: this.alg,
            input: {
                assetName: form.asset,
                VSName: `VS_Ret_${this.jobFormParams.optionsVS[form.vs]}_${form.horizon}d`,
                begDateTest: date.toISOString(),
                boolReplaceBase: form.replaceBase,
                boolReplaceModel: form.replaceModel
            }
        };
    }

    submit(data) {
        let payload = this.getPayloadBase(data);
        payload = this.getPayload(payload, data);

        this.projectsService.runJob(this.projectId, payload).subscribe(res => {
            this.router.navigate(['/app/project-list'], { queryParams: { project: this.projectId } });
        },
            err => {
                this.snackBar.open(`Error! (${err._body})`, 'Close', { duration: 5000 });
            });
    }

    ngOnInit() {
        this.algChabgeSubscription = this.projectsService.algorithmChangeObs.subscribe(alg => {
            this.alg = alg;
        });

        this.projectId = this.route.parent.snapshot.params['projectId'];
        this.projectsService.getAssets().subscribe(assetsData => {
            this.assetGroups = assetsData;
            const clone = this.route.snapshot.queryParams['clone'];

            if (clone) {
                this.restoreFormBase(clone, assetsData);
            } else {
                if (assetsData.length > 0) {
                    this.jobForm.controls['assetGroup'].setValue(assetsData[1].name);
                    this.changeAssets(assetsData[1].name);
                }
            }
        });
    }

    ngOnDestroy() {
        this.algChabgeSubscription.unsubscribe();
    }
}