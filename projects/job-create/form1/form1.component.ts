import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Observable, Subscription } from 'rxjs';
import { MdSnackBar } from '@angular/material';

import { ProjectsService } from './../../projects.service';

import { AbstractFormComponent } from './../job-create-abstract-form.component';
import { Job, JobInput, JobInputType1 } from './../../models';

class Slider {
    enabled: boolean;
    title: string;
    val: number;
    min: number;
    max: number;

    constructor(title: string) {
        this.enabled = true;
        this.title = title;
        this.val = 0;
        this.min = 0;
        this.max = 100;
    }
}

@Component({
    selector: 'app-job-create-form1',
    templateUrl: './form1.component.html'
})
export class Form1Component extends AbstractFormComponent {
    private sliders: {
        indices: Slider[],
        indicesTypes: Slider[]
    }

    constructor(protected projectsService: ProjectsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected formBuilder: FormBuilder,
        protected snackBar: MdSnackBar) {

        super(projectsService, route, router, formBuilder, snackBar);

        this.jobForm.addControl('mode', this.formBuilder.control('total'));

        this.sliders = {
            indices: this.jobFormParams.indices.map(title => new Slider(title)),
            indicesTypes: this.jobFormParams.indicesTypes.map(title => new Slider(title))
        }
    }

    restoreForm(jobParams): void {
        if (!!jobParams.input.DC_AssetsClassesPcts) {
            const setValuesForClone = (controlCollection) => {
                for (let x of controlCollection) {
                    if (jobParams.input.DC_AssetsClassesPcts[x.title]) {
                        x.val = jobParams.input.DC_AssetsClassesPcts[x.title];
                    } else {
                        x.enabled = false;
                    }
                };
            }

            setValuesForClone(this.sliders.indices);
            setValuesForClone(this.sliders.indicesTypes);

            if (jobParams.input.typeAssetsClassesPcts === 'InterClass') {
                this.jobForm.controls['mode'].setValue('total');
                this.processTotal(this.sliders.indices, null, true);
                this.processTotal(this.sliders.indicesTypes, null, true);
            } else if (jobParams.input.typeAssetsClassesPcts === 'IntraClass') {
                this.jobForm.controls['mode'].setValue('each');
            }
        }
    }

    onSliderValueChange(group, control): void {
        if (this.jobForm.controls['mode'].value == 'total') {
            this.processTotal(group, control, false);
        }
    }

    onChangeMode(): void {
        if (this.jobForm.controls['mode'].value == 'total') {
            this.processTotal(this.sliders.indices, null, false);
            this.processTotal(this.sliders.indicesTypes, null, false);
        } else {
            for (let x of this.sliders.indices) {
                x.max = 100;
            }

            Object.keys(this.sliders.indicesTypes).forEach(x => {
                this.sliders.indicesTypes[x].max = 100;
            });
        }
    }

    onSliderToggle(group, control): void {
        control.val = 0;
        if (this.jobForm.controls['mode'].value === 'total') {
            this.processTotal(group, control, false);
        }
    }

    // Maximum controls from group 'val' summary should be 100 or less
    processTotal(group, control, forced): void {
        let enabledControls = [];
        let total = 100;

        for (let x of group) {
            if (!forced && control === null) {
                x.val = 0;
                return;
            }

            if (x.enabled) {
                total = total - x.val;
                if (forced || x !== control) {
                    enabledControls.push(x);
                }
            } else {
                x.val = 0;
                enabledControls.push(x);
            }
        }

        if (!forced && control === null) {
            return;
        }

        if (enabledControls.length === 0) {
            return;
        }

        if (total < 0) {
            total = 0;
        }

        enabledControls.forEach(x => {
            x.max = x.val + total;
        });
    }

    getPayload(payload: Job, formData: any): Job {
        payload.formVersion = 1;

        const getSlidersValues = (sliders: Slider[], values: Object): Object => {
            for (let x of sliders) {
                if (x.enabled) {
                    values[x.title] = x.val;
                }
            }

            return values
        }

        let indicesValues: Object = getSlidersValues(this.sliders.indices, {});
        indicesValues = getSlidersValues(this.sliders.indicesTypes, indicesValues);

        let input = <JobInputType1>payload.input;
        input.DC_AssetsClassesPcts = indicesValues;
        input.typeAssetsClassesPcts = formData.mode === 'total' ? 'InterClass' : 'IntraClass';

        return payload;
    }
}