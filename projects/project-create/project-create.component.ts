import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

import { Project } from './../models/index';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-project-create',
    templateUrl: './project-create.component.html'
})
export class ProjectCreateComponent {
    projectForm: FormGroup;

    constructor(public dialogRef: MdDialogRef<ProjectCreateComponent>, formBuilder: FormBuilder) {
        this.projectForm = formBuilder.group({
            name: ['', [
                Validators.minLength(3),
                Validators.required
            ]],
            description: ['']
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    submit(project: Project): void {
        this.dialogRef.close(project);
    }
}
