import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

import { Observable } from 'rxjs';

import { ProjectsService } from './../projects.service';
import { UsersService } from './../../users/users.service';

@Component({
    selector: 'app-collaborators-manage',
    templateUrl: './collaborators-manage.component.html',
    providers: [UsersService]
})
export class CollaboratorsManageComponent implements OnInit {
    project: any;
    users: any[];

    constructor(private usersService: UsersService,
        private projectsService: ProjectsService,
        private dialogRef: MdDialogRef<CollaboratorsManageComponent>,
        @Inject(MD_DIALOG_DATA) private data: any) {
        this.project = data.project;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    submit(): void {
        this.dialogRef.close({});
    }

    addUser(user): void {
        this.projectsService.addCollaborator(this.project.id, user.UserId).subscribe(res => user.IsInProject = true);
    }

    removeUser(user): void {
        this.projectsService.removeCollaborator(this.project.id, user.UserId).subscribe(res => user.IsInProject = false);
    }

    ngOnInit() {
        let projectRequest = this.projectsService.getProject(this.project.id);
        let usersReqiest = this.usersService.getUsers();

        Observable.forkJoin([projectRequest, usersReqiest]).subscribe(([projects, users]: [any, any[]]) => {
            this.project = projects[0];

            let collaboratorsId = this.project.collaborators.map(u => u.id);
            this.users = users
                .filter(u => !u.IsAdmin && u.IsActive)
                .map(u => {
                    u.IsInProject = collaboratorsId.indexOf(u.UserId) > -1
                    return u;
                });
        });
    }
}
