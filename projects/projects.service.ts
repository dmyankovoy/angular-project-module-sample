import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { ApiService } from './../shared/api.service';

import {
    Project, Token, Algorithm, Asset, Job,
    JobResultData, JobChartDataItem
} from './models/index';

@Injectable()
export class ProjectsService {

    private algorithmChangeSub: Subject<any>;
    algorithmChangeObs: Observable<any>;

    constructor(private api: ApiService) {
        this.algorithmChangeSub = new Subject<any>();
        this.algorithmChangeObs = this.algorithmChangeSub.asObservable();
    }

    getProjects(): Observable<Project[]> {
        return this.api.get('cloudconnector/project')
            .map(res => <Array<Project>>res.json());
    }

    getProject(projectId: string): Observable<Project> {
        return this.api.get(`cloudconnector/project/${projectId}`)
            .map(res => <Project>res.json());
    }

    createProject(name: string, description: string): Observable<any> {
        return this.api.post(`cloudconnector/project`, {
            name: name,
            description: description
        });
    }

    getToken(projectId: string): Observable<Token> {
        return this.api.get(`cloudconnector/project/${projectId}/token`)
            .map(res => res.json());
    }

    addCollaborator(projectId: number, userId: number): Observable<any> {
        return this.api.put(`cloudconnector/project/${projectId}/user/${userId}`);
    }

    removeCollaborator(projectId: number, userId: number): Observable<any> {
        return this.api.delete(`cloudconnector/project/${projectId}/user/${userId}`);
    }

    updateProject(projectId: number, name: string, description: string): Observable<any> {
        return this.api.put(`cloudconnector/project/${projectId}`,
            {
                name: name,
                description: description
            });
    }

    getAssets(): Observable<Asset[]> {
        return this.api.get(`cloudconnector/static/assets`)
            .map(res => res.json());
    }

    getAlgs(): Observable<Algorithm[]> {
        return this.api.get(`cloudconnector/static/algorithms`)
            .map(res => res.json());
    }

    getJobs(projectId: string): Observable<Job[]> {
        return this.api.get(`cloudconnector/project/${projectId}/job`)
            .map(res => res.json());
    }

    runJob(projectId: string, params): Observable<any> {
        return this.api.post(`cloudconnector/project/${projectId}/run`, params);
    }

    getJobInfo(projectId: string, jobId: string): Observable<Job> {
        return this.api.get(`cloudconnector/project/${projectId}/job/${jobId}`)
            .map(res => res.json()[0]);
    }

    getJobFiles(jobId: string): Observable<string[]> {
        return this.api.get(`cloudconnector/job/${jobId}/result/files`)
            .map(res => res.json());
    }

    getFIleContentChart(jobId: string, file: string) {
        return this.api.get(`cloudconnector/job/${jobId}/result/0/json`, { file: file })
            .map(res => res.json());
    }

    getJobStats(jobId: string, file: string) {
        return this.getFIleContentChart(jobId, `Stats${file}`);
    }

    getJobResults(jobId: string, files: string[]): Observable<JobResultData[]> {
        let filesRequestObs: Observable<any>[] = [];

        for (let fileName of files) {
            filesRequestObs.push(this.getFIleContentChart(jobId, fileName));
            filesRequestObs.push(this.getJobStats(jobId, fileName));
        }

        return Observable.forkJoin(filesRequestObs).map((filesContent: any[]) => {
            let idx = 0;
            let jobResultsData: JobResultData[] = [];

            while (filesContent.length > 0) {
                const resultData = new JobResultData();

                resultData.fileName = files[idx];

                let chartData = filesContent.shift();
                resultData.chart = chartData.map(x => {
                    return new JobChartDataItem(x['PnL'], x['RdtStrategy'], x['__Dates'], x['__Pred'], x['__Target']);
                });

                resultData.stat = filesContent.shift();

                jobResultsData.push(resultData);
                idx++;
            }

            return jobResultsData;
        });
    }

    getJobLogs(jobId: string): Observable<string> {
        return this.api.get(`cloudconnector/job/${jobId}/logs`)
            .map(res => res.text());
    }

    deleteJob(jobId: string): Observable<any> {
        return this.api.delete(`cloudconnector/job/${jobId}`);
    }

    selectAlgorithm(key: string): void {
        this.algorithmChangeSub.next(key);
    }
}