export class JobChartDataItem {
    pnL: number;
    rdtStrategy: string;
    date: Date;    
    pred: number;
    target: number;

    constructor(pnL: string, rdtStrategy: string, date: string, pred: string, target: string ) {
        this.pnL = parseFloat(pnL);
        this.rdtStrategy = rdtStrategy;
        this.date = new Date(date);
        this.pred = parseFloat(pred);
        this.target = parseFloat(target);
    }
}