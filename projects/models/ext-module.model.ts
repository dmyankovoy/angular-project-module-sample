export interface ExtModule {
    id: string;
    name: string;    
    system: boolean;
    description: string
}