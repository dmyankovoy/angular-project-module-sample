import { Datasource } from './datasource.model';
import { User } from './user.model';

export class Project {
    id: string;
    createdAt: Date;
    creator: User;
    name: string;
    description: string;
    datasource: Datasource;
    updatedAt: Date;    
    collaborators: User[]
}