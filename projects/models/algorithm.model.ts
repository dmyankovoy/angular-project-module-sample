export interface Algorithm {    
    name: string;
    formVersion: number;
    description: string;
}