import { JobInput } from './job-input.model'

export interface JobInputType0 extends JobInput {
    method: string,
    negNoise: number,
    posNoise: number,
    negClass: number,
    posClass: number
}