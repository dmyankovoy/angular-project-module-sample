import { JobChartDataItem } from './job-data-chart.model';

export class JobResultData {
    fileName: string;
    chart: JobChartDataItem[];
    stat: Object[];
}