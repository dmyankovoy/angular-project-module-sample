export interface Datasource {
    id: string;    
    name: string;
    path: string;
}