export enum AssetTypes {
    category,
    asset
}

export interface Asset {    
    name: string;
    kind: AssetTypes;
    items: Asset[];
}