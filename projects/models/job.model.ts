import { Datasource } from './datasource.model';
import { User } from './user.model';
import { JobInput } from './job-input.model';
import { JobStates } from './job-states';

export interface Job {
    id: string;
    created: Date;
    creator: User;
    formVersion: number;
    algorithm: string;
    input: JobInput,
    state: JobStates;
    target: string[][];
}