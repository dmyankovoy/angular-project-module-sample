export interface JobInput {
    VSName: string;
    assetName: string;    
    begDateTest: Date;
    boolReplaceBase: boolean;
    boolReplaceModel: boolean;
}