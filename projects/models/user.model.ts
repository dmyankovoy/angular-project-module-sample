import { ExtModule } from './ext-module.model';

export interface User {
    id: string;
    created: Date;
    name: string;
    email: string;
    isActive: boolean;
    isAdmin: boolean;    
    modules: ExtModule[]
}