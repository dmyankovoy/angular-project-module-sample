
export interface Token {
    datasource: string;
    expired: Date;
    id: string;
    user: string;
}